import React, { Fragment } from "react";
import Nav from "../../components/admin-nav/index";
import SignIn from "../../components/admin-signin/index";
import "./Styles.css";
const Home = () => {
  const TitleNav = "POLLERÍA EL SUPER DORADO-SESTEMA DE GESTIÓN DE DELIVERY";
  return (
    <Fragment>
      <div className="width-100 height-100 h-pr-fl-ma">
        <div className="width-100 height-10vh background-plate-gradient h-pr-fl-ma">
          <Nav TitleNav={TitleNav} />
        </div>
        <div className="width-100 height-80vh background-plate-gradient h-pr-fl-ma">
          <div className="width-60 mobile-display-none height-100 h-pr-fl-ma">
            <div className="width-100 height-50 h-pr-fl-ma relative-vertical-center">
              <h1 className="width-70 height-70 centered">aca va el logo</h1>
            </div>
          </div>
          <div className="mobile-width-100-child-1 width-40 height-100 mobile-width-100 mobile-relative-horizontal-center h-pr-fl-ma background-plate-gradient">
            <SignIn />
          </div>
        </div>
        <div className="width-100 height-10vh background-white-gradient-2 h-pr-fl-ma">
          <div className="width-100 height-auto centered h-pr-fl-ma">
            <div className="width-100 text-align-center h-pr-fl-ma font-size-13px current-font current-color-3 font-weight-bold">
              | POLLERÍA EL DORADO | Un proyecto de MINIMAL CODE | Developed by
              Team Tinku - 2020 LATAM |
            </div>
            <div className="width-100 text-align-center h-pr-fl-ma font-size-12px current-font current-color-3 font-weight-bold">
              | Términos y condiciones de uso | Soporte de errores | Contacto |
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Home;
