import React, { Fragment } from "react";
import Nav from "../../components/admin-nav/index";
import RightMenu from "../../components/right-menu/index";
import SecPendiente from "../../components/sec-pendiente/index";
import InfoPedido from "../../components/Info-pedido/index";
const Dashboard = () => {
  return (
    <Fragment>
      <div className="width-95 height-100  h-pr-fl-ma overflow-y-hidden ">
        <div className="width-100 height-10vh h-pr-fl-ma">
          <Nav TitleNav="SISTEMA GESTION DE PEDIDOS" />
        </div>
        <div className="width-100 height-90vh h-pr-fl-ma ">
          <div className="width-25 height-100 h-pr-fl-ma">
            <div className="width-100 height-10 h-pr-fl-ma">
              <div className=" width-5 height-30 margin-top-20  border-radius-25 h-pr-fl-ma background-skyblue-gradient"></div>
              <div className="width-95 height-100 border-radius-25 h-pr-fl-ma">
                <span className="special-font-size-13px centered">
                  PENDIENTE
                </span>
              </div>
            </div>
            <div className="width-100 height-90 h-pr-fl-ma">
              <SecPendiente />
            </div>
          </div>
          <div className="width-25 height-100 h-pr-fl-ma">
            <div className="width-100 height-50 h-pr-fl-ma">
              <div className="width-100 height-20 h-pr-fl-ma">
                <div className=" width-5 height-30 margin-top-20  border-radius-25 h-pr-fl-ma background-yellow"></div>
                <div className=" width-95 height-100  h-pr-fl-ma ">
                  <span className="special-font-size-13px centered">
                    EN CAMINO
                  </span>
                </div>
              </div>
              <SecPendiente />
            </div>
            <div className="width-100 height-50 h-pr-fl-ma linea-boton">
              <div className="width-100 height-20 h-pr-fl-ma">
                <div className=" width-5 height-30 margin-top-20  border-radius-25 h-pr-fl-ma background-red-node"></div>
                <div className=" width-95 height-100  h-pr-fl-ma ">
                  <span className="special-font-size-13px centered">
                    NO ACEPTADO
                  </span>
                </div>
              </div>
              <SecPendiente />
            </div>
          </div>
          <div className="width-50 height-100 h-pr-fl-ma">
            <InfoPedido />
          </div>
        </div>
      </div>
      <div className="width-5 height-100vh background-black-node h-pr-fl-ma overflow-y-hidden ">
        <div className="width-100 height-10  h-pr-fl-ma linea-boton ">
          <span className="width-100 height-100 border-none current-color-2 background-none h-pr-fl-ma ">
            <i className="fas fa-align-justify centered"></i>
          </span>
        </div>
        <div className="width-100 height-90  h-pr-fl-ma ">
          <div className="width-100 height-15  h-pr-fl-ma ">
            <span className="width-100 height-100 border-none current-color-2 background-none h-pr-fl-ma ">
              <i className="fas fa-desktop centered"></i>
            </span>
          </div>
          <div className="width-100 height-15  h-pr-fl-ma ">
            <span className="width-100 height-100 border-none current-color-2 background-none h-pr-fl-ma ">
              <i className="fas fa-clipboard-list centered"></i>
            </span>
          </div>
          <div className="width-100 height-15  h-pr-fl-ma ">
            <span className="width-100 height-100 border-none current-color-2 background-none h-pr-fl-ma ">
              <i className="far fa-address-card centered"></i>
            </span>
          </div>
          <div className="width-100 height-15  h-pr-fl-ma ">
            <span className="width-100 height-100 border-none current-color-2 background-none h-pr-fl-ma ">
              <i className="fas fa-archive centered"></i>
            </span>
          </div>
          <div className="width-100 height-15  h-pr-fl-ma ">
            <span className="width-100 height-100 border-none current-color-2 background-none h-pr-fl-ma ">
              <i className="fas fa-chart-pie centered"></i>
            </span>
          </div>
          <div className="width-100 height-25  h-pr-fl-ma ">
            <span className="width-100 height-100 border-none background-none current-color-2 h-pr-fl-ma ">
              <i className="fas fa-cogs centered"></i>
            </span>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Dashboard;
