import React, { Fragment } from "react";

const InfoPedido = () => {
  return (
    <Fragment>
      <div className="width-100 height-100  h-pr-fl-ma">
        <div className="width-100 height-10  h-pr-fl-ma">
          <span className="special-font-size-13px centered">
            PEDIDO SELECCIONADO
          </span>
        </div>
        <div className="width-100 height-90  h-pr-fl-ma">
          <div className="width-50 height-90  h-pr-fl-ma">
            <div className="width-100 height-30 margin-10 h-pr-fl-ma">
              <div className="width-100 height-40  h-pr-fl-ma">
                <div className="width-50 height-100  h-pr-fl-ma">
                  <span>Carlos Abrahan</span>
                </div>
                <div className="width-50 height-100  h-pr-fl-ma">
                  <span>929483618</span>
                </div>
              </div>
              <div className="width-100 height-30  h-pr-fl-ma">
                <div className="width-50 height-100  h-pr-fl-ma">
                  <span>AV. TITO JAIME #304</span>
                </div>
                <div className="width-50 height-100  h-pr-fl-ma">
                  <span>S/.50</span>
                </div>
              </div>
              <div className="width-100 height-30  h-pr-fl-ma">
                <div className="width-50 height-100  h-pr-fl-ma">
                  <div className=" width-5 height-20 border-radius-25 h-pr-fl-ma background-skyblue-gradient"></div>
                  <div className="width-95 height-100 centered h-pr-fl-ma ">
                    <span>Pendiente</span>
                  </div>
                </div>
                <div className="width-50 height-100  h-pr-fl-ma">
                  <span>hace un minuto</span>
                </div>
              </div>
            </div>
            <div className="width-100 height-70  h-pr-fl-ma">
              <div className="width-100 height-20  h-pr-fl-ma">
                <span className="centered">LISTA DE PRODUCTOS</span>
              </div>
              <div className="width-95 height-70 margin-10 border-radius-10 chat-color h-pr-fl-ma">
                <div className="margin-10">
                  <span>-POLLO A LA BRASA ENTERTO(1)</span>
                </div>
              </div>
              <div className="width-100 height-10  h-pr-fl-ma">
                <div className="width-50 height-100  h-pr-fl-ma">
                  <button className="text-align-center relative-horizontal-center outline-none width-80 height-35px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover current-color-2 border-none color-white font-weight-bold ">
                    Preparar
                  </button>
                </div>
                <div className="width-50 height-100  h-pr-fl-ma">
                  <button className="text-align-center relative-horizontal-center outline-none width-80 height-35px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover  color-white current-color-2 border-none font-weight-bold ">
                    NO ACEPTAR
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="width-50 height-90  h-pr-fl-ma">
            <div className="width-90 height-100 chat-color centered h-pr-fl-ma border-radius-10">
              <div className="width-100 height-10  h-pr-fl-ma">
                <span className="centered">NOTIFICAR AL CLIENTE</span>
              </div>
              <div className="width-100 height-95  h-pr-fl-ma ">
                <div className="width-90 height-70 border-radius-10 margin-left-15 h-pr-fl-ma ">
                  <div className="width-95   height-10 margin-10 footer-chat-color border-radius-10 h-pr-fl-ma">
                    <span className="centered">SU PEDIDO ESTÁ EN CAMINO</span>
                  </div>
                </div>
                <div className="width-100 height-30  h-pr-fl-ma footer-chat-color border-radius-bottom-right-10 border-radius-bottom-left-10">
                  <div className="width-100 height-60  h-pr-fl-ma ">
                    <div className="width-100 height-50   h-pr-fl-ma">
                      <button className="text-align-center relative-horizontal-center centered outline-none width-95 height-30px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover chat-color current-color-2 font-weight-bold border-none">
                        SU PEDIDO ESTÁ EN CAMINO
                      </button>
                    </div>
                    <div className="width-100 height-50  h-pr-fl-ma">
                      <button className="text-align-center relative-horizontal-center centered  outline-none width-95 height-30px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover chat-color current-color-2 font-weight-bold border-none">
                        LO SENTIMOS, NO PODEMOS ATENDERLOS
                      </button>
                    </div>
                  </div>
                  <div className="width-100 height-40  h-pr-fl-ma">
                    <div className="width-80 height-100   h-pr-fl-ma ">
                      <input
                        tyep="text"
                        name="mensage"
                        placeholder="Ingrese su mensaje"
                        className="input text-align-center  centered height-40px border-radius-10 chat-color border-none current-font font-weight-bold font-size-13px current-color-2  width-90 outline-none"
                        required
                      />
                    </div>
                    <div className="width-20 height-100  h-pr-fl-ma">
                      <button className="text-align-center centered current-color-2 relative-horizontal-center outline-none width-80 height-35px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover chat-color  color-white font-weight-bold border-none ">
                        <i className="far fa-arrow-alt-circle-right" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default InfoPedido;
