import React, { Fragment } from "react";

const SecPendiente = () => {
  return (
    <Fragment>
      <div className="width-80 height-15 margin-10 h-pr-fl-ma border-radius-10 background-white-gradient-2 ">
        <div className="width-100 height-50 h-pr-fl-ma ">
          <div className="width-50 height-100 h-pr-fl-ma  ">
            <div className="margin-5 centered">
              <span>DIRECCION</span>
            </div>
          </div>
          <div className="width-50 height-100 h-pr-fl-ma ">
            <div className="margin-5 centered">
              <span>S/. 50</span>
            </div>
          </div>
        </div>
        <div className="width-100 height-50 h-pr-fl-ma ">
          <div className="width-50 height-100 h-pr-fl-ma ">
            <div className="margin-5 centered">
              <span className="center">9:30 PM</span>
            </div>
          </div>
          <div className="width-50 height-100 h-pr-fl-ma ">
            <div className="margin-5 centered">
              <span className="center">01/06/2020</span>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default SecPendiente;
