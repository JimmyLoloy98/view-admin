import React, { Fragment } from "react";

const Nav = (props) => {
  return (
    <Fragment>
      <div className="width-100 height-10vh background-plate-gradient h-pr-fl-ma">
        <div className="width-20 height-100 h-pr-fl-ma ">
          <span className="width-70 height-70 centered">logo</span>
        </div>
        <div className="width-80 height-100 h-pr-fl-ma">
          <div className="width-80 height-100 centered h-pr-fl-ma relative-horizontal-center">
            <span className="special-font-size-20px centered">
              {props.TitleNav}
            </span>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Nav;
