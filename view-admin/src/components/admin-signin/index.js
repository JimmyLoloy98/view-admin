import React, { Fragment } from "react";

const Signin = () => {
  return (
    <Fragment>
      <div className="width-80 height-100 mobile-width-100 mobile-relative-horizontal-center h-pr-fl-ma overflow-hidden relative-horizontal-center">
        <div className="width-100 height-10">
          <span className="centered special-font-size-20px">
            INICIO DE SESIÓN
          </span>
        </div>
        <div className="width-100 height-90">
          <form>
            <div className="margin-20">
              <label htmlFor="Username" className="special-font-size-13px">
                Nombre de Usuario
              </label>
              <input
                type="text"
                placeholder="Insert your userName"
                name="Username"
                required
                className="input text-align-center height-40px border-radius-10 current-background-2 current-font font-weight-bold font-size-13px current-color-2 border-none width-100 outline-none"
              />
            </div>
            <div className="margin-20">
              <label htmlFor="Password" className="special-font-size-13px">
                Contraseña
              </label>
              <input
                type="password"
                placeholder="Insert your password"
                name="Password"
                required
                className="input text-align-center height-40px border-radius-10 current-background-2 current-font font-weight-bold font-size-13px current-color-2 border-none width-100 outline-none"
              />
            </div>
            <div className="width-100 height-auto margin-top-10 h-pr-fl-ma relative-horizontal-center ">
              <div className="width-50  height-auto h-pr-fl-ma">
                <button className="text-align-center relative-horizontal-center outline-none width-80 height-35px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover buton-outline color-white font-weight-bold ">
                  INGRESAR
                </button>
              </div>
              <div className="width-50  height-auto h-pr-fl-ma">
                <button className="text-align-center relative-horizontal-center outline-none width-80 height-35px h-pr-fl-ma border-radius-10 cursor-pointer current-background-emphasis-1-hover buton-outline color-white font-weight-bold ">
                  ATRÁS
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </Fragment>
  );
};

export default Signin;
