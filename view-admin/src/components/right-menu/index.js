import React, { Fragment } from "react";

const RightMenu = () => {
  return (
    <Fragment>
      <div>
        <span>MENU</span>
      </div>
      <div>
        <span>ADMINISTRACION DE PEDIDOS</span>
      </div>
      <div>
        <span>ADMINISTRACION DE PRODUCTOS</span>
      </div>
      <div>
        <span>PEDIDOS REGISTRADOS</span>
      </div>
      <div>
        <span>CLIENTES REGISTRADOS</span>
      </div>
      <div>
        <span>ESTADISTICAS</span>
      </div>
      <div>
        <span>CONFIGURACION</span>
      </div>
    </Fragment>
  );
};
export default RightMenu;
